###Paddy Sapphire

import discord
import os
import random
import asyncio
from discord.ext import commands
from apscheduler.schedulers.asyncio import AsyncIOScheduler

bot = commands.Bot(
    command_prefix='pad.',
    allowed_mentions=discord.AllowedMentions(
            users=False,         # Whether to ping individual user @mentions
            everyone=False,      # Whether to ping @everyone or @here mentions
            roles=False,         # Whether to ping role @mentions
            replied_user=False,  # Whether to ping on replies to messages
        ),)

msgcache_running=[]
msgcache=[]
paddy_active=True

@bot.event
async def on_ready():
    print("I have a vision that I am logged in as {0.user}!".format(bot))


@bot.event
async def on_guild_join(guild):
    await guild.system_channel.send("I have a vision that someone will invite me to a server!")

@bot.event
async def on_message(message):
    if message.author == bot.user:
        return
    if message.author.bot: return

    if len(message.attachments)>0:
        for i in message.attachments:
            if i.content_type.startswith("image")==False:
                return


    msgcache_running.append(message)
    if message.content.startswith("pad."):
        msgcache_running.remove(message)


    if 'hello paddy' in message.content :
        await message.channel.send("I have a feeling someone will greet me!")


    await bot.process_commands(message)


reactioncount=5

@bot.event
async def on_reaction_add(reaction, user):
    if user == bot.user:
        return

    if reaction.message.author == bot.user:
        return

    if reaction.count >= reactioncount:
        msgcache.append(reaction.message)

    if len(reaction.message.reactions) >= reactioncount:
        msgcache.append(reaction.message)



async def vision():
    global msgcache_running
    if msgcache_running==[]:
        return
    msg = random.choice(msgcache_running)
    visions_nopic=[f'I have a vision that {msg.author.name} will say " {msg.content} ". Interesting!']






    await bot.wait_until_ready()
    c = bot.get_channel(msg.channel.id)



    if len(msg.attachments)>0:

        if msg.content=="":
            await c.send(f'I have a vision that {msg.author.name} send a picture, but they don\'t say anything. Mysterious! {msg.attachments[0].url}')
            return

        visions_pic=[f'I have a vision that {msg.author.name} will say "{msg.content}", and send a picture! {msg.attachments[0].url}']
        await c.send(random.choice(visions_pic))

    else:
        await c.send(random.choice(visions_nopic))
    msgcache_running=[]


async def vision_reactions():
    global msgcache
    if msgcache==[]:
        return
    msg = random.choice(msgcache)
    visions_nopic=[f'I have a vision that {msg.author.name} will say " {msg.content} ". Interesting!']


    await bot.wait_until_ready()
    c = bot.get_channel(msg.channel.id)

    if len(msg.attachments)>0:

        if msg.content=="":
            await c.send(f'I have a vision that {msg.author.name} send a picture, but they don\'t say anything. Mysterious! {msg.attachments[0].url}')
            return

        visions_pic=[f'I have a vision that {msg.author.name} will say " {msg.content} ", and send a picture! {msg.attachments[0].url}']
        await c.send(random.choice(visions_pic))

    else:
        await c.send(random.choice(visions_nopic))
    msgcache.remove(msg)





scheduler = AsyncIOScheduler()
scheduler.add_job(vision, 'interval', hours=2, id='visions')
scheduler.add_job(vision_reactions, 'interval',seconds=random.randint(1800,5400), id='vision_reactions')
scheduler.start()

randnum = random.randint(0,59)

##Utils cmds
@bot.command()
async def set_freq(ctx, hour, minute=randnum, second=randnum):
    '''
    Changes the time frequency that Paddy makes a vision
    (H M S) format with no parentheses
    If you want to enable/disable the bot use toggle
    If you want to change interval of reaction visions use set_interval
    Type default to set frequency to regular frequency
    '''
    global scheduler
    if hour == "default":
        scheduler.reschedule_job('visions',trigger='interval', hours=2, minutes=0, seconds=0)
        await ctx.send(f'I have a vision that you\'d like my visions at the default frequency!')
        return
    scheduler.reschedule_job('visions',trigger='interval',hours=int(hour), minutes=int(minute), seconds=int(second))

    if minute == randnum or second == randnum:
        await ctx.send(f"My frequency has been changed to every {hour} hour(s)!")
        return

    await ctx.send(f"My frequency has been changed! I will now make a vision every {hour} hours, {minute} minutes, and {second} seconds.")

@bot.command()
async def set_interval(ctx, num1, num2):

    '''
    Changes the time interval in minutes that Paddy makes a vision from a reaction
    (n1 n2) format with no parnetheses
    If you want to enable/disable the bot use toggle
    If you want to change frequency of visions use set_freq
    Type default to set intervals to default
    '''
    if num1=="default":
        scheduler.reschedule_job("vision_reactions", trigger="interval", seconds=random.randint(1800,5400))
        await ctx.send(f'I have a vision that you\'d like my visions at the default intervals!')
        return

    if num1>num2:
        await ctx.send("Please put the smaller number first!")
        return
    randn = random.randint(int(num1)*60,int(num2)*60)
    scheduler.reschedule_job("vision_reactions", trigger="interval", seconds=randn)
    await ctx.send(f'My vision interval has been changed from {num1} to {num2} minutes!')


@bot.command()
async def toggle(ctx):
    '''Turn Paddy\'s visions on and off.'''
    global paddy_active
    if paddy_active:
        await ctx.send(f'I have a vision that you no longer want any of my visions...')
        scheduler.pause()
        paddy_active=False
        return

    if paddy_active==False:
        await ctx.send(f'I have a vision that you would like for me to continue my visions!')
        scheduler.resume()
        paddy_active=True
        return

@bot.command()
async def reactions(ctx, num):
    '''Changes the number of reactions needed to log an event
    '''
    global reactioncount
    reactioncount = int(num)
    await ctx.message.add_reaction("👍")



## Debug cmds
@bot.command()
async def ping(ctx):
    '''Bot returns latency
    '''
    await ctx.send(f'I have a vision that my latency is **{round(bot.latency*1000)}ms!**')

@bot.command()
async def return_running(ctx):
    "Returns messages in running cache (for visions)"
    print(msgcache_running)

#
#
@bot.command()
async def return_cache(ctx):
    "Returns messages in cache (for reaction visions)"
    print(msgcache)

#
#
@bot.command()
async def return_visions(ctx):
    "Returns possible visions"

@bot.command()
async def return_attachment_type(ctx):
    global msgcache
    for x in msgcache:
        for i in x.attachments:
            print(i.content_type)








TOKEN = "[your token here]"
bot.run(TOKEN)
