# Padparadscha Sapphire

A joke Discord bot based off of Padparadscha Sapphire from SU. Every so often, she will have "visions" of a post already posted in you server.

## Installation
Paddy is currently ran locally. If you would like to download the code file and put your bot's token where it says [insert token here] at the bottom.

## Authors and acknowledgment
Made by shimzini

## License
Free for anyone to use and share.

## Project status
Overall complete, but I will come back to patch this here and there. I made this code open source though so if you want to make changes feel free to :)
